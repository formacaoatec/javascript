# Exercício de avaliação de VueJS

## Instalação da API

### Clone do repoditório
```
git clone https://bitbucket.org/formacaoatec/javascript.git
```

### Instalar dependências
```
composer install
```

### Criação da base de dados e configuração
Após criar a base de dados, configure o ficheiro .env

### Executar migrations
```
php artisan migrate --seed
```

### Arrancar servidor
```
php artisan serve
```
## Uso da API

### Obter todos os contactos
```
GET /api/contacts
```

### Obter um contacto específico
```
GET /api/contacts/{id}
```

### Criar um contacto
```
POST /api/contacts
{
    "name": "Lopes da Silva",
    "email": "lopesdasilva@atec.pt",
    "phone_number": "+351123456789",
    "cell_number": "+351987654321"
}
```

### Modificar um contacto
```
PUT /api/contacts/{id}
{
    "name": "Lopes da Silva",
    "email": "lopesdasilva@atec.pt",
    "phone_number": "+351123456789",
    "cell_number": "+351987654321"
}
```

### Eliminar um contacto
```
DELETE /api/contacts/{id}
```
