<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Validator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Contact::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|required',
            'phone_number' => 'required',
            'cell_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }

        $contact = new Contact;

        $contact->name = $request->input("name");
        $contact->email = $request->input("email");
        $contact->phone_number = $request->input("phone_number");
        $contact->cell_number = $request->input("cell_number");
        $contact->save();

        return response("Contacto criado com sucesso", 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::find($id);

        if($contact) {
            return $contact;
        }

        return response('Contacto não existente!', 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|required',
            'phone_number' => 'required',
            'cell_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }
        
        $contact = Contact::find($id);
        
        if($contact) {
            $contact->name = $request->input("name");
            $contact->email = $request->input("email");
            $contact->phone_number = $request->input("phone_number");
            $contact->cell_number = $request->input("cell_number");
            $contact->save();

            return response("Contacto atualizado com sucesso", 200);
        }

        return response('Contacto não existente!', 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        
        if($contact) {
            $contact->delete();

            return response("Contacto eliminado com sucesso", 200);
        }

        return response('Contacto não existente!', 404);
    }
}
